# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Product.create([
  {name: "Guava", description:"Fruit", price: 80.0},
  {name: "Pineapple", description:"Fruit", price: 55.0},
  {name: "Mango", description:"Fruit", price: 110.0},
  {name: "Watermelon", description:"Fruit", price: 50.0}])