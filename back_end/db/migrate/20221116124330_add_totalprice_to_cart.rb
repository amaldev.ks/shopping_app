class AddTotalpriceToCart < ActiveRecord::Migration[7.0]
  def change
    add_column :carts, :totalprice, :integer
  end
end
