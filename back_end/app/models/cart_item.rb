class CartItem < ApplicationRecord
  belongs_to :product
  belongs_to :cart
  validates :quantity, numericality: true
  
  #total price of item
  def total_price
    product.price * quantity
  end
end
