class User < ApplicationRecord
  has_one :cart, dependent: :destroy
  devise :database_authenticatable,
         :jwt_authenticatable,
         :registerable,
         jwt_revocation_strategy: JwtDenylist


  enum role: { admin: 0, user: 1}
  scope :without_cart, -> {User.where(cart: nil)}
end
