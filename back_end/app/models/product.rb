class Product < ApplicationRecord
  has_many :cart_items, dependent: :destroy
  has_many :carts, through: :cart_items, source: :cart

  validates :name, uniqueness: true
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :name, presence: true, allow_blank: false, length: { minimum: 1, maximum: 500 }
  validates :description, presence: true, allow_blank: false, length: { minimum: 2 }
  
end
