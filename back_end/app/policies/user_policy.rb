class UserPolicy < ApplicationPolicy

  def admin?
  	user.admin?
  end

  def user?
    user.user?
  end
end