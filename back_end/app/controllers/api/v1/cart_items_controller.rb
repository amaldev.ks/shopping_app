class Api::V1::CartItemsController < ApplicationController
  before_action :set_cart_item, only: %i[ show update destroy ]

  #create cart item
  def create
    @cart_item = CartItem.new(cart_item_params)
    if @cart_item.save
      cart = Cart.find(params[:cart_id])
      total_cart_price = cart.cart_items.pluck(:total_price).sum
      cart.update(totalprice: total_cart_price)
      render json: @cart_item, status: :created
    else
      render json: @cart_item.errors, status: :unprocessable_entity
    end
  end

  #update item quantity and price
  def update
    if @cart_item.update(cart_item_params)
      cart_item = CartItem.find(params[:id])
      total_cart_price = cart_item.cart.cart_items.pluck(:total_price).sum
      cart_item.cart.update(totalprice: total_cart_price)
      render json: @cart_item
    else
      render json: @cart_item.errors, status: :unprocessable_entity
    end
  end

  #remove cart item
  def destroy
    @cart_item.destroy
  end

  private
    #find cart item
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    #set permitted cart item params
    def cart_item_params
      params.require(:cart_item).permit(:product_id, :cart_id, :quantity, :total_price)
    end
end
