class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: %i[ show update destroy ]

  #list all users
  def index
    binding.pry
    authorize current_user, :admin?
    @users = User.all.without(current_user)

    render json: @users
  end

  #show user data
  def show
    authorize current_user, :user? 
    render json: @user
  end

  #update user
  def update
    authorize current_user, :user?
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  #destroy user
  def destroy
    authorize current_user, :admin?
    @user.destroy
  end

  private

    #find user
    def set_user
      @user = User.find(params[:id])
    end

    #permit user params
    def user_params
      params.require(:user).permit(:email, :username, :password, :password_confirmation)
    end
end
