class Api::V1::SessionsController < Devise::SessionsController
  respond_to :json

  #Login user
  def create
    resource = User.find_by_email(params[:email])
    if resource && resource.valid_password?(params[:password])
      @cart_id = resource.cart.id
      @current_user = resource
      render json: { message: 'Successfully Logged In.', resource: { user_info: resource, cart_id:@cart_id} }, status: :ok
    else
      render json: { errors: { 'email or password' => ['is invalid'] } }, status: :unprocessable_entity
    end
  end

  private

    #logout response
    def respond_to_on_destroy
      render json: { message: "Logged out.", head: :ok }, status: :ok
    end

    #permit signin params
    def sign_in_params
      params.require(:session).permit(:email, :password)
    end

end
