import React from 'react'
import axios from "axios";
import { useEffect, useState, useCallback } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import './Cart.css'
  
  export default function Cart() {
    const user = JSON.parse(localStorage.getItem('user'));
    const cart_id = user.cart_id
    console.log(cart_id);
    const [state,setState] = useState([])
    const [priceData, setPriceData] = useState(null);
    useEffect(() => {
	 	 axios.get(`http://localhost:3000/api/v1/carts/${cart_id}`).then((response)=>{
			  setState(response.data)
        totalPriceHandler();
		  })
	  }, [])

    function updateItemQuantity(cart_item_id, quantity, price) {
      let itemQuantity = quantity + 1;
      let itemPrice = itemQuantity * price;
      axios
      .patch(`http://127.0.0.1:3000/api/v1/cart_items/${cart_item_id}`, { quantity: itemQuantity, total_price: itemPrice })
      .then((res) => {
        console.log(res);
        axios.get(`http://localhost:3000/api/v1/carts/${cart_id}`).then((response)=>{
          setState(response.data)
          totalPriceHandler();
        })
      });
    }
    function removeItemQuantity(cart_item_id, quantity, price) {
      let itemQuantity = quantity - 1;
      let itemPrice = itemQuantity * price;
      axios
      .patch(`http://127.0.0.1:3000/api/v1/cart_items/${cart_item_id}`, { quantity: itemQuantity, total_price: itemPrice })
      .then((res) => {
        console.log('result', res);
        axios.get(`http://localhost:3000/api/v1/carts/${cart_id}`).then((response)=>{
          setState(response.data)
          totalPriceHandler();
        })
      });
    }

    function deleteItemQuantity(cart_item_id) {
      axios
      .delete(`http://127.0.0.1:3000/api/v1/cart_items/${cart_item_id}`)
      .then((res) => {
        console.log(res);
        axios.get(`http://localhost:3000/api/v1/carts/${cart_id}`).then((response)=>{
          setState(response.data)
        })
      });
    }
    const totalPriceHandler = useCallback(() => {
      if (cart_id){
        axios.get(`http://localhost:3000/api/v1/carts/${cart_id}/total_price`)
        .then(res => {
          if (res && res.data) {
            console.log('res', res);
            setPriceData(res.data);
          }
        })
        .catch(err => {
          if (err && err.response) {
            console.log('err', err.response);
          }
        });
      }
    }, []);  
  return (
    <div className='cart-items'>
    <div className='cart-items-header'>Cart Items</div>
    <div>
      {state.map((cart_list, index) =>(
        <div key={index} className="cart-items-list">
          <img src="https://images.pexels.com/photos/1132047/pexels-photo-1132047.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                                      className="cart-items-image"/>
          <div className='cart-items-name'>{cart_list.product.name}</div>
          <div className='cart-items-name'>${cart_list.item.total_price}</div>
          <div className='cart-items-function'>
            <button className=" cart-items-add "
              onClick={() => removeItemQuantity(cart_list.item.id, cart_list.item.quantity, cart_list.product.price, {flag: "dec"})}>
              <i className="fas fa-minus"></i>
            </button>
            <div>
              <input id="form1" min="0" name="quantity" value={cart_list.item.quantity} type="text" className="cart-list-item-quantity" readOnly= {true}/>
              
            </div>
            <button className='cart-items-remove'
              onClick={() => updateItemQuantity(cart_list.item.id, cart_list.item.quantity, cart_list.product.price, {flag: "inc"})}>
              <i className="fas fa-plus"></i>
            </button>
            <button className='cart-items-delete text-right'
              onClick={() => deleteItemQuantity(cart_list.item.id)}>
              <i className="fas fa-trash"></i>
            </button>
          </div>
        </div>
      ))}
      <div className='cart-items-total-price-name'>
        Total Prize
        <div className='cart-items-total-price'> ${priceData}</div>
      </div>
    </div>
  </div>
);
};
