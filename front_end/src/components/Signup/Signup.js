import React, {useState} from 'react'
import swal from 'sweetalert';

// importing css file
import "./Signup.css"

async function signupUser(credentials) {
  return fetch('http://127.0.0.1:3000/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json())
 }
 
 export default function Signup() {
  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [confirmpassword, setConfirmpassword] = useState();
  

  const handleSubmit = async e => {
    e.preventDefault();
    const response = await signupUser({
      username,
      email,
      password,
      confirmpassword
    });
    console.log('response', response);
    if ('resource' in response) {
      swal("Success", response.message, "success", {
        buttons: false,
        timer: 2000,
      })
      .then((value) => {
        localStorage.setItem('resource', response['resource']);
        localStorage.setItem('user', JSON.stringify(response['resource']));
        console.log(response['resource']['cart_id']);
        localStorage.setItem('userData', response['resource']['cart_id']);
        window.location.href = "/products";
      });
    } else {
      swal("Failed", response.message, "error");
    }
  }
  return (
    <div className="signup">
      <h1>Sign Up</h1>
      <form onSubmit={handleSubmit}>
          <input type={'username'} placeholder={'Username'} onChange={(e)=>setUsername(e.target.value)} />
          <input type={'email'} placeholder={'Email'} onChange={(e)=>setEmail(e.target.value)} />
          <input type={'password'} placeholder={'Password'} onChange={(e)=>setPassword(e.target.value)} />
          <input type={'confirmpassword'} placeholder={'Confirm password'} onChange={(e)=>setConfirmpassword(e.target.value)} />
          <button type={'submit'}>Sign up</button>
      </form>
    </div>
  );
}
