// importing css file
import "./NavigationBar.css";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import React from "react";
import axios from "axios";


const NavigationBar = () => {
  const user = JSON.parse(localStorage.getItem('user'));
    const cart_id = user.cart_id
    console.log(cart_id);
    const [state,setState] = useState([])
    const WAIT_TIME = 5000;
    useEffect(() => {
      const id = setInterval(() => {
      axios.get(`http://localhost:3000/api/v1/carts/${cart_id}`).then((response)=>{
          setState(response.data)
        })
      }, WAIT_TIME);
    })
  return (
    <header className="header">
      <div>
        <h1>
          <Link to="/products" className="logo">
            E-Commerce App
          </Link>
        </h1>
      </div>
      <div className="header-links">
        <ul>
          <li>
            <Link to="/">Welcome</Link>
          </li>
        </ul>
        <ul>
          <li>
            <Link to="/cart" className="cart">
              <i className="fas fa-shopping-cart" />
              <span className="cart-length">
                  {state.length}
              </span>
            </Link>
          </li>
        </ul>
      </div>
    </header>
)};

export default NavigationBar;