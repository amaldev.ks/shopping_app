import React, {useState} from 'react'
import swal from 'sweetalert';
// importing css file
import "./Login.css"

async function loginUser(credentials) {
  return fetch('http://127.0.0.1:3000/users/sign_in', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json())
 }
 
 export default function Signin() {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async e => {
    e.preventDefault();
    const response = await loginUser({
      email,
      password
    });
    console.log('response', response);
    if ('resource' in response) {
      swal("Success", response.message, "success", {
        buttons: false,
        timer: 2000,
      })
      .then((value) => {
        localStorage.setItem('resource', response['resource']);
        localStorage.setItem('user', JSON.stringify(response['resource']));
        console.log(response);
        localStorage.setItem('resourceData', JSON.stringify(response['cart_id']));
        window.location.href = "/products";
      });
    } else {
      swal("Failed", response.message, "error");
    }
  }
  return (
    <div className="login">
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
          <input type={'email'} placeholder={'Email'} onChange={(e)=>setEmail(e.target.value)} />
          <input type={'password'} placeholder={'Password'} onChange={(e)=>setPassword(e.target.value)} />
          <button type={'submit'}>Login</button>
      </form>
    </div>
  );
}
