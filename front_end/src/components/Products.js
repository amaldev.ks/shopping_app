import React from 'react'
import axios from "axios";
import { useEffect, useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import './Products.css'

function Products() {
    const [state,setState] = useState([])
	const handleAddProduct = (cartitem_id, price) =>{
		const cart = JSON.parse(localStorage.getItem('user'));
		alert('Added to cart successfully');
		console.log('cart id', cart.cart_id);
		console.log('Click happened');
		axios
		.post('http://127.0.0.1:3000/api/v1/cart_items', { cart_id: cart.cart_id, product_id: cartitem_id, quantity: 1, total_price: price })
		.then((res) => {
			console.log(res);
		});
	}
	useEffect(() => {
	 	 axios.get('http://localhost:3000/api/v1/products').then((response)=>{
			  setState(response.data)
		  })
	}, [])
  return (
    <div className='products'>
        {state.map((obj)=>{
		  return(
			<div className='card'>
				<div>
					<img src="https://images.pexels.com/photos/1132047/pexels-photo-1132047.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                                      className="w-100" />
				</div>
				<div>
					<h3 className='product-name'>{obj.name}</h3>
				</div>
				<div>
					<h3 className='product-price'> ${obj.price}</h3>
				</div>
				<div>
					<button className='product-add-button' onClick={() => handleAddProduct(obj.id, obj.price)}>
						Add to Cart
					</button>
				</div>
			</div>
		  )
		})}
    </div>
  )
}

export default Products