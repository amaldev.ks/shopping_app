import './App.css';
import LoginSignupContainer from './components/LoginSignupContainer/LoginSignupContainer';
import NavigationBar from './components/NavigationBar/NavigationBar';
import Products from './components/Products';
import Cart from "./components/Cart/Cart"
import { Routes, Route, Navigate } from "react-router-dom";
import { useState } from 'react';



const App = () => {
  const [cartItems, setCartItems] = useState([]);

  const handleAddProduct = (obj) =>{
    const ProductExist = cartItems.find((item) => item.id === obj.id);
    if(ProductExist){
      setCartItems(cartItems.map((item) => item.id === obj.id ?
      {...ProductExist, quantity: ProductExist.quantity + 1}: item)
      );
      }
    else{
      setCartItems([...cartItems, {...obj, quantity: 1}])
    }
  };

  const handleRemoveProduct = (obj) => {
    const ProductExist = cartItems.find((item) => item.id === obj.id);
    if(ProductExist.quantity === 1){
      setCartItems(cartItems.filter((item) => item.id !== obj.id));
    } else{
      setCartItems(
        cartItems.map((item) => item.id === obj.id ? {...ProductExist, quantity: ProductExist.quantity - 1} : item)
      )
    }
  }
  return (
    <div className="App">
      <NavigationBar />
      <Routes>
        <Route path="/" element={<Navigate to="/login" />} />
        <Route path="/login" element={<LoginSignupContainer />} />
        <Route path="/products" element={<Products />} />
        <Route path="/cart" element={<Cart />} />
      </Routes>
    </div>
  );
};

export default App;
